<?php
        $db = new SQLite3('../../partials/database2.db');
        $results = $db->query('SELECT * FROM product');
        
    ?>

<!DOCTYPE html>
<html lang="fr">

<html>

<head>
    <title>Nos Burgers - Le Simplonien</title>
    <html lang="fr">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../CSS/stylesheet.css">
    <link rel="icon" type="image/png" href="/images/logos-simplonien/favicon.png">
    <script type="text/javascript" src="../JS/script.js"></script>

</head>

<body>

    <header>
        <?php
        include "../../partials/header.php"
        ?>
    </header>


    <nav id="nav-main">
        <?php
        include "../../partials/nav.php"
        ?>
    </nav>

    <main id="main-equipe">
        <h1>Produits</h1>
        <div class="bodypro">

            <div id="img-product-wrapper">
                <img id="img-produc" src="../images/frites.jpg" alt="Frites maisons" type="button" data-toggle="modal"
            data-target="#avatarBig">
                <img class="img-product" src="../images/steak.jpg" alt="100% boeuf" type="button" data-toggle="modal"
            data-target="#avatarBig">
                <img class="img-produc" src="../images/pains.jpg" alt="Pains à burger frais artisan boulanger" type="button" data-toggle="modal"
            data-target="#avatarBig">
            </div>

            <div class="bouton">
                <button class="prev" onclick="prev()">&lt;</button>
                <img id="burger" src="../images/cheezy.jpg" width="100%" height="100%" />
                <button class="next" onclick="next()">&gt;</button>
            </div>

            <script>
                var product = ["../images/vegetarien.jpg", "../images/cheezy.jpg", "../images/patriot.jpg", "../images/burgers.jpg", "../images/lunatic.jpg"];
                var num = 0;

                function next() {
                    var burger = document.getElementById("burger");
                    num++;
                    if (num >= 4) {
                        num = 0;
                    }
                    burger.src = product[num];
                }

                function prev() {
                    var burger = document.getElementById("burger");
                    num--;
                    if (num < 0) {
                        num = 3;
                    }
                    burger.src = product[num];
                }
            </script>


            <?php 
            while ($row = $results->fetchArray()) { 
                ?>
            <div id="vegetarian" class="vegetarian">
                <h2 class="title11">
                    <?php echo $row['Title']; ?>
                </h2>
                <img class="img11" src="<?php echo $row['image']; ?>" alt="<?php echo $row['alt']; ?>">
                <p class="des11">
                    <?php echo $row['content'];  ?>
                </p>
            </div>
            <?php } ?>




        </div>



        <div class="menu">
            <h2 class="title12">Le Menu :</h2>
            <img class="menu-img" src="../images/menu.png" alt="Menu">
        </div>
    </main>
    <footer>
        <?php
        include "../../partials/footer.php"
        ?>
    </footer>
</body>
<label class="switch">
    <input type="checkbox" id="checkbox" class="l" onchange="darkmode();" autocomplete="off">
    <span class="slider round"></span>

</html>