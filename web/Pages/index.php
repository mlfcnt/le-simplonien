<?php
$db = new SQLite3('../../partials/database2.db');
$results = $db->query('SELECT id, title, content, image FROM event WHERE last = 0 ORDER BY RANDOM() LIMIT 1');
?>

<!DOCTYPE html>
<html>

<head>
    <title>Accueil - Le Simplonien</title>
    <html lang="fr">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../CSS/stylesheet.css">
    <link rel="icon" type="image/png" href="../images/logos-simplonien/favicon.png">
    <script type="text/javascript" src="../JS/script.js"></script>
</head>

<body>

    <header>
        <?php include("../../partials/header.php");
    ?>
    </header>
    <nav id="nav-main">
        <?php include("../../partials/nav.php");
    ?>
    </nav>
    <main id="main-equipe">
        <h1>Le simplonien</h1>
        <article>

            <div id="container-jour">
                <div id="photo-jour"></div>

                <div id="description-jour">
                    <h2 id="burger-jour" class="centre">Le Végétarien</h2>
                    <p>Vous êtes végétarien et aimez les recettes maison et les hamburger? Nous vous présentons
                        ainsi cette recette de burger végétarien au steak de carotte. En effet ce burger végétal
                        est composé d'un steak de légumes aux carottes et sauce soja. Ainsi ces steaks de carottes
                        remplaceront de façon avantageuse le traditionnel steak haché tout en vous permettant de
                        consommer peu de matière grâce à cette version plus diététique.
                    </p>
                </div>
            </div>
        </article>

        <article id="container-2">

            <div id="wrapper-instagram">
                <div><img src="../images/instagram/insta1.jpg" alt="image cheese-burger"></div>
                <div><img src="../images/instagram/insta2.jpg" alt="image plongeur"></div>
                <div><img src="../images/instagram/insta3.jpg" alt="image lunatic"></div>
                <div><img src="../images/instagram/insta4.jpg" alt="image chips"></div>
                <div><img src="../images/instagram/insta5.jpg" alt="image Robert"></div>
                <div><img src="../images/instagram/insta6.jpg" alt="image Double-burger"></div>
                <div><img src="../images/instagram/insta7.jpg" alt="image cheese special"></div>
                <div><img src="../images/instagram/insta8.jpg" alt="image chevre"></div>
                <div><img src="../images/instagram/insta9.jpg" alt="image menu"></div>
                <div><img src="../images/instagram/insta10.jpg" alt="image tomatoes burger"></div>
                <div><img src="../images/instagram/insta11.jpg" alt="image oignons"></div>
                <div><img src="../images/instagram/insta12.jpg" alt="image salad"></div>
                <div><img src="../images/instagram/insta13.jpg" alt="image cuistot"></div>
                <div><img src="../images/instagram/insta14.jpg" alt="image multiple"></div>
                <div><img src="../images/instagram/insta15.jpg" alt="image cheesy"></div>
                <div><img src="../images/instagram/insta16.jpg" alt="image burger"></div>
            </div>

            <div id="evenement">
                <h2 class="eve">Actualité</h2>

                <?php 

while ($row = $results ->fetchArray()){ 
$a = 'UPDATE event SET last =1 WHERE id ='.$row['id'].'';
$db->query($a);
$a = 'UPDATE event SET last =0 WHERE id !='.$row['id'].'';
$db->query($a);
    ?>
                <h3 id="titre-evenement">
                    <?php
    echo $row['title'];
 ?>
                </h3>
                <div id="description-evenement">
                    <?php echo $row['content']; ?>
                </div>
                <div id="image-evenement">
                    <img src="<?php echo $row['image']; } ?>" alt="Evenement_du_jour">
                </div>
            </div>
        </article>




    </main>
    <footer>
        <?php include "../../partials/footer.php";?>
    </footer>
</body>
<label class="switch">
    <input type="checkbox" id="checkbox" class="l" onchange="darkmode();" autocomplete="off">
    <span class="slider round"></span>

</html>