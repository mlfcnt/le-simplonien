<?php
$db = new SQLite3('../../partials/database2.db');
$results = $db->query('SELECT * FROM team WHERE onglet="athlete"');
?>
<!DOCTYPE html>

<html>

<head>
    <title>L'équipe - Le Simplonien</title>
    <html lang="fr">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../CSS/stylesheet.css">
    <link rel="icon" type="image/png" href="/images/logos-simplonien/favicon.png">
    <script type="text/javascript" src="../JS/script.js"></script>
</head>



<body>

    <header>
        <?php include '../../partials/header.php'; ?>

    </header>
    <nav id="nav-main">
        <?php include '../../partials/nav.php'; ?>
    </nav>
    <main id="main-equipe">
        <h1>L'équipe</h1>
        <div id="nav-equipe">
            <ul>
                <li id="first-li-nav-equipe" onclick="Toggle1()">Athlètes</li>
                <li id="2nd-li-nav-equipe" onclick="Toggle2()">Fournisseurs</li>
                <li id="3rd-li-nav-equipe" onclick="Toggle3()">Soutiens</li>
            </ul>
        </div>
        <div id="wrapper-equipe">
            <?php while ($row = $results->fetchArray()): ?>

            <div class="photo-equipe">
                <img src="<?php echo $row ['image'];?>" alt="<?php echo $row['alt']; ?>"> </div>
            <div class="desc-equipe">
                <p><strong>
                        <?php echo $row ['first_name']. ' ' . $row ['last_name'];  ?></strong><br>
                    <?php echo $row ['description1']; ?>
                </p>
                <p>
                    <?php echo $row ['description2']; ?>
                </p>
            </div>
            <?php endwhile; ?>

        </div>


    </main>

    <footer>
        <?php 
            include "../../partials/footer.php";
        ?>
    </footer>
</body>
<label class="switch">
    <input type="checkbox" id="checkbox" class="l" onclick="darkmode();" autocomplete='off'>
    <span class="slider round"></span>

</html>