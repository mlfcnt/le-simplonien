<?php
    $db = new SQLite3('../../partials/database2.db');
    //$mysql_connect('localhost','root','');
    //$mysql_finalco('database2', $mysql_connect);
    
    //si le formulaire est rempli et envoyé, transfère dans la bdd
    if (isset($_POST['buttonEnvoyer'])) //isset = Détermine si une variable est déclarée et est différente de NULL    
    {
        $name = $_POST['user_name'];
        $email = $_POST['user_email'];
        $subject = $_POST['user_sujet'];
        $message = $_POST['user_message'];
        $created_date = date('Y-m-d H:i:s');
        $db->exec("INSERT INTO contact (created_date,name,email,subject,content) VALUES ('$created_date','$name', '$email', '$subject', '$message')");
    }    
?>

<!DOCTYPE html>

<head>
    <title>Contact - Le Simplonien</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../CSS/stylesheet.css">
    <link rel="icon" type="image/png" href="/images/logos-simplonien/favicon.png">
    <script type="text/javascript" src="../JS/script.js"></script>
</head>
<body>

    <header>
        <?php 
            include "../../partials/header.php";
        ?>
    </header>
    <nav id="nav-main">
        <?php
            include "../../partials/nav.php";
        ?>
    </nav>
    <main id="main-equipe">
        <h1>Contact</h1>
        <div class="cohab-hours-map">
            <div id="hours">
                <h3>Horaires d'ouverture</h3>
                <p>12:00 / 14:00 et 19:00 / 22:00 du Lundi au Samedi</p>
            </div>
            <div id="map">
                <h3>Plan d'accès</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2793.1737261135713!2d5.9162818509900745!3d45.566944934500455!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478ba8f8a216697b%3A0xc21a6a228af35180!2sThe+Roster+Chambery+Centre!5e0!3m2!1sfr!2sfr!4v1548946164380"
                    width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

        </div>



        <div class="contact">
            <h3>Contactez-nous</h3>
            
            <form action="contact.php" method="post">

                <div>
                    <label for="name">Votre nom (requis)</label><br>
                    <input class="name" type="text" id="name" name="user_name" required><br>
                </div>
                <div>
                    <label for="email">Votre email (requis)</label><br>
                    <input class="email" type="email" id="email" name="user_email" required><br>
                </div>
                <div>
                    <label for="sujet">Sujet (requis)</label><br>
                    <input class="sujet" type="text" id="sujet" name="user_sujet" required><br>
                </div>
                <div>
                    <label for="message">Votre message</label><br>
                    <textarea class="message" rows=10 type="text" name="user_message"></textarea>
                    <br>
                    <label for="button"></label><br>
                    <input class="button" name="buttonEnvoyer" id="button" value="Envoyer" type="submit">

                </div>
                    
                

            </form>
        </div>
    </main>
    <footer>
        <?php include "../../partials/footer.php";?>
    </footer>
</body>
<label class="switch">
    <input type="checkbox" id="checkbox" class="l" onchange="darkmode();" autocomplete="off">
    <span class="slider round"></span>

    </html>