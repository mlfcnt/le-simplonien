# Le Simplonien (restaurant de Burger factice  basé à Chambery)


# Genèse
- Date de création du projet : 31 janvier 2019.
- Equipe : Aurore, Marie, Mickael et Tommy

# Process de création 
## Répartition du Travail 

1) **Groupe** : Choix du thème (burger) et template (theroster.fr/web).
2) **Groupe** : Répartition des pages : Index (Aurore), Product (Mick), Team (Tommy) et Contact (Marie)
3) **Aurore et Mick** : Dessiner sur papier la structure de chaque page. (blueprint)
4) **Marie et Tommy** : Création du repos Git, upload sur Gitlab et création des pages index, team, contact, product (html) et stylesheet (css).
5) **Groupe** : Création d'une branche dev pour tester nos pages avant des les merge à la branche master.
6)  **Groupe** : Config du git sur chacun des PC des membres du groupe.
7) **Individuel** : Chaque membre du groupe crée la structure de sa propre page (HTML) en essayant de s'approcher au maximum de la source (theroster.fr)
8) **Groupe** : Création des blocs communs à toutes les pages : header, nav et footer
9) **Groupe** : remplacement des blocs communs à toutes les pages : header, nav et footer par une instruction php + création de fichiers spécifiques en php
10) **Groupe** : réorganisation de la branche dev avec 2 répertoires : "web" avec les pages .php + les images et "partials" avec les parties communes (footer.php...)
11) **Groupe** : ajout de scripts JS sur le site (défilement d'images, onglets internes ex team.php)
12) **Individuel** : construction d'une base de données avec 4 tables nécessaires à chacune des pages, insertion de requetes sur chacune des pages
13) **Groupe** : prise en compte des remarques de l'évaluation du site, correction en vue de l'accessibilité
14) **Groupe** : nettoyage et restructuration de l'architecture du site

# Structure
## Pages

### - **index.html** (Aurore)
- **objectif** : Page d'accueil du site
- **structure** : header et nav (communs aux autres pages), main (spécifique) et footer (commun)
- ```<main>```comprend deux ```<article>```: 1) "burger du jour" + 2) un élement "instagram" avec 16 mini photos + un élément "événement" qui flotte à coté
- **éléments** :
- ```#nav-main``` : identique aux autres pages
- ```#main-equipe``` : identique aux autres pages met en forme le main (fond blanc, ombrage, dimensions)
- ```#container-jour``` : contient une photo, un titre et un texte
- ```#photo-jour``` : place de la photo
- ```#description-jour``` : place du texte de description
- ```#burger-jour``` : place du titre
- ```#instagram``` : nom de l'article
- ```#wrapper-instagram``` : div contenant les 16 photos format grip
- ```#evenement``` : nom de l'article qui flotte
- ```#description evenement``` : place de la description de l'évenement
- ```#photo-evenement``` : place de la photo de l'évenement


### - **product.html** (Mick)
- **objectif** : Présenter les prosuits
- **structure** : Ma page comprend 5 éléments photos et 4 éléments texte
- **éléments** : 
```<bodypro>``` et ```<main>``` : Comprend tout le corps de la page PRODUITS.  
```<img-product>``` et ```<img-poduct1>``` : comprend les 3 premières images.  
```<produits>``` : Comprend tout les textes et images des produits.  
```<vegetarian>``` et ```<montagnard>``` et ```<homer>``` et ```<duo>``` : comprend le titre une image et un texte du produit corespondant.  
```<title11>``` et ```<des11>``` : Correspond aux titre et texte de gauche.  
```<title12>``` et ```<des12>``` : Correspond aux titre et texte de droite.  
```<img11>``` et ```<img12>``` et ```<img13>``` : Correspond a chaque image des produits.  
```<menu>``` : comprend l'image du menu et sont titre.  

### - **team.html** (Tommy)
- **objectif** : Présenter les athlètes sponsorisés par The Roster. (Bonus si on a le temps : présenter les fournisseurs et soutiens)
- **structure** : Ma page comprend 2 éléments photos et 2 éléments texte (qui décrivent les photos). J'ai décidé de faire un tableau qui comprend 4 cases avec CSS Grid.
- **éléments** :   
```<main>``` : comprend tout le corps de ma page team  
```<nav-equipe>``` : petite nav pour se balader entre les 3 sous parties de la page (L'équipe, Fournisseurs et Soutiens)   
```<wrapper-equipe>``` : wrapper css grid (comprend les 2 ```<photo-equipe>``` et les 2 ```<desc-equipe>```)  
```<photo-equipe1> ``` : css grid item  
```<desc-equipe1> ``` : css grid item  
```<photo-equipe2> ``` : css grid item  
```<desc-equipe2> ``` : css grid item  

### - **contact.html** (Marie)

- **objectif** : Présenter les horaires et jours d'ouverture, le plan accès (intégration d'une GoogleMap) et un formulaire de contact
- **structure** : 
- Deux parties : 
- La première qui contient les horaires d'ouverture et la map Google
- La seconde qui contient un formulaire de contact
- **éléments** : 
- ```<div class="cohab-hours-map">``` un grid pour la premiere partie hours/map
- ```<div class="contact">``` form action pour la seconde
- 
# Points à travailler pour un projet ulterieur
- préparer davantage le projet en amont, notamment la structure du projet, l'artitechture (wireframe)
- éviter de copier-coller les parties communes : utiliser php
- Créer un fichier php unique pour appeler la database
- Utiliser plutôt les classes pour css et id pour js
- pas de style, ni de script dans le HTML
- toujours identifier la page sur laquelle on est
- Dans git, créer une page par développeur
- Veiller à l'accessibilité dès le début.