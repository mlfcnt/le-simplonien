# 1. Images
Principe WCAG : Perceptible
Recommandation :
Donner à chaque image porteuse d'information une alternative textuelle pertinente et une description détaillée si nécessaire. Lier les légendes à leurs images. Remplacer les images textes par du texte stylé lorsque c'est possible.
### Critère 1.1 [A] Chaque image a-t-elle une alternative textuelle ?
oui
### Critère 1.2 [A] Pour chaque image de décoration ayant une alternative textuelle, cette alternative est-elle vide ?
oui
### Critère 1.3 [A] Pour chaque image porteuse d'information ayant une alternative textuelle, cette alternative est-elle pertinente (hors cas particuliers) ?
oui
### Critère 1.4 [A] Pour chaque image utilisée comme CAPTCHA ou comme image-test, ayant une alternative textuelle, cette alternative permet-elle d'identifier la nature et la fonction de l'image ?
nc
### Critère 1.5 [A] Pour chaque image utilisée comme CAPTCHA, une solution d'accès alternatif au contenu ou à la fonction du CAPTCHA est-elle présente ?
nc
### Critère 1.6 [A] Chaque image porteuse d'information a-t-elle, si nécessaire, une description détaillée ?
- Ex les burgers : Texte à coté de description, mais sans longdesc ou autre
- pas pour l’entete

### Critère 1.7 [A] Pour chaque image porteuse d'information ayant une description détaillée, cette description est-elle pertinente ?
idem
### Critère 1.8 [AA] Chaque image texte porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?
Pas stylé, pas toujours un texte
### Critère 1.10 [A] Chaque légende d'image est-elle, si nécessaire, correctement reliée à l'image correspondante ?
Pas de légendes => description

# 3. Couleurs
### Critère 3.1 [A] Dans chaque page web, l'information ne doit pas être donnée uniquement par la couleur. Cette règle est-elle respectée ?
Oui

### Critère 3.2 [A] Dans chaque page web, l'information ne doit pas être donnée uniquement par la couleur. Cette règle est-elle implémentée de façon pertinente ?
Oui

### Critère 3.3 [AA] Dans chaque page web, le contraste entre la couleur du texte et la couleur de son arrière-plan est-il suffisamment élevé (hors cas particuliers) ?
Non 



# 6. Liens
Principe WCAG : Perceptible
Recommandation :
Donner des intitulés de lien explicites, grâce à des informations de contexte notamment, et utiliser le titre de lien le moins possible.
### Critère 6.1 [A] Chaque lien est-il explicite (hors cas particuliers) ?
La majorité des lien = une photo 
les autres : oui
### Critère 6.2 [A] Pour chaque lien ayant un titre de lien, celui-ci est-il pertinent ?
Oui
### Critère 6.4 [A] Pour chaque page web, chaque lien identique a-t-il les mêmes fonction et destination ?
Nc pas de lien identiques sur une meme page
### Critère 6.5 [A] Dans chaque page web, chaque lien, à l'exception des ancres, a-t-il un intitulé ?
Il y a une image

# 8. Éléments Obligatoires

### Critère 8.1 [A] Chaque page web est-elle définie par un type de document ?
Type de document
Ensemble de données de référence qui permet aux agents utilisateurs de connaître les caractéristiques techniques des langages utilisés sur la page web (balise doctype).

### Critère 8.2 [A] Pour chaque page web, le code source est-il valide selon le type de document spécifié (hors cas particuliers) ?

### Critère 8.3 [A] Dans chaque page web, la langue par défaut est-elle présente ?

### Critère 8.4 [A] Pour chaque page web ayant une langue par défaut, le code de langue est-il pertinent ?

### Critère 8.5 [A] Chaque page web a-t-elle un titre de page ?

### Critère 8.6 [A] Pour chaque page web ayant un titre de page, ce titre est-il pertinent ?

### Critère 8.7 [AA] Dans chaque page web, chaque changement de langue est-il indiqué dans le code source (hors cas particuliers) ?

### Critère 8.8 [AA] Dans chaque page web, chaque changement de langue est-il pertinent ?

### Critère 8.9 [A] Dans chaque page web, les balises ne doivent pas être utilisées uniquement à des fins de présentation. Cette règle est-elle respectée ?

### Critère 8.10 [A] Dans chaque page web, les changements du sens de lecture sont-ils signalés ?

# 9. Structuration de l'information
### 9.1 Critère 9.1 [A] Dans chaque page web, l'information est-elle structurée par l'utilisation appropriée de titres ?
Non. Pas de balises h1

### 9.2 Critère 9.2 [A] Dans chaque page web, la structure du document est-elle cohérente ?
Pas de main 

### 9.3 Critère 9.3 [A] Dans chaque page web, chaque liste est-elle correctement structurée ?
Ul li oui 
list listitem non 

### Critère 9.4 [AAA] Dans chaque page web, la première occurrence de chaque abréviation permet-elle d'en connaître la signification ?
Non

### Critère 9.5 [AAA] Dans chaque page web, la signification de chaque abréviation est-elle pertinente ?
X

### Critère 9.6 [A] Dans chaque page web, chaque citation est-elle correctement indiquée ?
Non



# 10. Présentation de l'information

### Critère 10.1 [A] Dans le site web, des feuilles de styles sont-elles utilisées pour contrôler la présentation de l'information ?
oui

### Critère 10.2 [A] Dans chaque page web, le contenu visible reste-t-il présent lorsque les feuilles de styles sont désactivées ?
Le texte oui, certaines images non (exemple accueil)

### Critère 10.3 [A] Dans chaque page web, l'information reste-t-elle compréhensible lorsque les feuilles de styles sont désactivées ?
Oui dans l’ensemble

### Critère 10.4 [AA] Dans chaque page web, le texte reste-t-il lisible lorsque la taille des caractères est augmentée jusqu'à 200%, au moins (hors cas particuliers) ?
Dans l’ensemble, sauf présentation slide le texte saute, ex footer le texte saute...

### Critère 10.5 [AA] Dans chaque page web, les déclarations CSS de couleurs de fond d'élément et de police sont-elles correctement utilisées?
Non il est ajouté aussi dans le HTML

### Critère 10.6 [A] Dans chaque page web, chaque lien dont la nature n'est pas évidente est-il visible par rapport au texte environnant ?
Bof. Texte en petit en rouge

### Critère 10.7 [A] Dans chaque page web, pour chaque élément recevant le focus, la prise de focus est-elle visible ?
Non pas du tout ex le menu

### Critère 10.13 [A] Pour chaque page web, les textes cachés sont-ils correctement affichés pour être restitués par les technologies d'assistance ?
Le alt est vide

### Critère 10.14 [A] Dans chaque page web, l'information ne doit pas être donnée uniquement par la forme, taille ou position. Cette règle est-elle respectée ?
Par marqueur visuel pr le menu, 
Par la couleur pour les images qui peuvent être cliquées (plus sombres) si survolées etc

### Critère 10.15 [A] Dans chaque page web, l'information ne doit pas être donnée par la forme, taille ou position uniquement. Cette règle est-elle implémentée de façon pertinente ?
Non exemple images taille précisée dans HTML

## 12 Navigation

### -Critère 12.1 [AA] Chaque ensemble de pages dispose-t-il de deux systèmes de navigation différents, au moins (hors cas particuliers) ?
Non

### -Critère 12.2 [AA] Dans chaque ensemble de pages, le menu et les barres de navigation sont-ils toujours à la même place (hors cas particuliers) ?
Oui


### -Critère 12.3 [AA] Dans chaque ensemble de pages, le menu et les barres de navigation ont-ils une présentation cohérente (hors cas particuliers) ? 
Oui

### -Critère 12.4 [AA] La page « plan du site » est-elle pertinente ? 
Oui

### -Critère 12.5 [AA] Dans chaque ensemble de pages, la page « plan du site » est-elle atteignable de manière identique ?
Oui

### -Critère 12.6 [AA] Dans chaque ensemble de pages, le moteur de recherche est-il atteignable de manière identique ? 
Non pas de barre de recherhce

### -Critère 12.7 [AA] Dans chaque page d'une collection de pages, des liens facilitant la navigation sont-ils présents ? 
Oui


### -Critère 12.10 [A] Dans chaque page web, les groupes de liens importants (menu, barre de navigation…) et la zone de contenu sont-ils identifiés ?
Oui

### -Critère 12.11 [A] Dans chaque page web, des liens d'évitement ou d'accès rapide aux groupes de liens importants et à la zone de contenu sont-ils présents (hors cas particuliers) ? 
Oui
	

### -Critère 12.13 [A] Dans chaque page web, l'ordre de tabulation est-il cohérent ? 
Oui

### -Critère 12.14 [A] Dans chaque page web, la navigation ne doit pas contenir de piège au clavier. Cette règle est-elle respectée ? 
Non
