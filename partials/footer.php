<?php 
echo "
<div id=\"wrapper-footer\">
    <div id=\"footer-title\">Heures D'ouverture</div>
    <div id=\"footer-adresse1\">
        <strong>ANNECY CENTRE</strong><br>
        2 Rue Joseph Blanc<br>
        LUNDI AU DIMANCHE<br>
        12:00 / 22:00 </div>
    <div id=\"footer-adresse2\">
        <strong>ANNECY LE VIEUX</strong><br>
        2 Rue Jean Mermoz<br>
        LUNDI AU SAMEDI<br>
        12:00 / 14:00<br>
        19:00 / 22:00</div>
    <div id=\"footer-adresse3\">
        <strong>MEYTHET</strong><br>
        2 Route de FRANGY<br>
        LUNDI AU SAMEDI<br>
        12:00 / 14:00<br>
        19:00 / 22:00
    </div>
    <div id=\"footer-adresse4\">
        <strong>CHAMBERY</strong><br>
        15 Rue JP Veyrat<br>
        LUNDI AU SAMEDI<br>
        12:00 / 14:00<br>
        19:00 / 22:00
    </div>
    <div id=\"footer-adresse5\">
        <strong>CHAMBERY CHAMNORD</strong><br>
        1097, avenue des Landiers<br>
        LUNDI AU DIMANCHE<br>
        11:30 / 14:00<br>
        19:00 / 22:00
    </div>
    <div id=\"footer-sponsor1\"><img src=\"../images/logo-vans.jpg\"alt=\"logo-vans\"></div>
    <div id=\"footer-sponsor2\"><img src=\"../images/logo-volcom.jpg\"alt=\"logo-volcom\"></div>
    <div id=\"footer-sponsor3\"><img src=\"../images/logo-nixon.jpg\"alt=\"logo-nixon\"></div>
    <div id=\"footer-sponsor4\"><img src=\"../images/logo-electric.jpg\"alt=\"logo-electric\"></div>
    <div id=\"footer-sponsor5\"><img src=\"../images/logo-pullin.jpg\"alt=\"logo-pullin\"></div>

    <div id=\"footer-social\">
    <a href=\"https://www.facebook.com/Lifestyleburgers?fref=ts\" target=\"_blank\">
    <img src=\"../images/facebook-footer.jpg\" alt=\"facebook-footer\" width=\"22\" height=\"22\" style=\"opacity: 1;\"></a> | 
    <a href=\"http://instagram.com/the_roster\" target=\"_blank\">
    <img src=\"../images/instagram-footer.jpg\"alt=\"instagram-footer\" width=\"22\" height=\"22\" style=\"opacity: 1;\"></a> | 
    <a href=\"http://theroster.fr/web/?page_id=589\">PRESSE</a></div>

    <div class=\"container\" id=\"footer-copyright\">
        <p class=\"vertical\"><strong>Copyright Le Simplonien - <a href=\"https://www.theroster.fr/politique-de-confidentialite/\"
                    target=\"_blank\">Politique de confidentialité</a></strong></p>
    </div>
</div>
";
?>
